# set, the game #

Set is a card game, where the goal is to find a triple of cards matching certain criteria
See https://en.wikipedia.org/wiki/Set_(card_game) for all details.
The winner is who find the triplets the fastest.
So now we are going to do that in Python!
Find a routine that does this:
Given a list of numbers (0 through 80) that encode the cards.
The routine must return the indices in this list for three numbers that form a set.
And, faster is better!

### Why? ###

I wrote it for fun, but I realized this turned into a Python optimization tutorial.
So, read the code, play with it yourself, and try to find out what is fast, and why!
You'll see that the fastest way is more than a hundred times faster than the slowest!

### How do I get set up? ###
Numpy is needed for a few of the routines,
maybe I'll remove this dependency in a later version.
Apart from that, it is a single file program that you just run.

### Contribution guidelines ###

If you find a routine that is faster than the fastest one, or otherwise interesting, let me know,
because you deserve to be in the credits!
I would be really impressed, and your name will be added to the code.

Jurjen