#!python3
"""set game in python by Jurjen N.E. Bos
See https://en.wikipedia.org/wiki/Set_(card_game) for the rules
Challenge:
make a function with inputs:
   table: list of 3-20 input numbers, in range 0-80
   (using the obvious numbering where the ternary digits are the four properties)
   output: indices in table of set items, if there is a set, else None
   Of course, just like in the real game, faster is better!
   here are a few implementations I found (using way too much time)
"""

import random, string
from itertools import combinations, repeat, product, chain, tee
import time
import numpy as np
from operator import itemgetter
from collections import defaultdict, Counter
from pickle import dumps

digitstring = string.digits + string.ascii_lowercase

def shuffleDeck(n=81):
    """Create shuffled deck.
    >>> random.seed(25)
    >>> shuffleDeck(5)
    [2, 1, 4, 0, 3]
    """
    result = list(range(n))
    random.shuffle(result)
    return result

def toBase(n, base=3, length=0):
    """write out n in any number base
    >>> toBase(123)
    '11120'
    """
    result = ''
    while n or length > 0:
        n, d = divmod(n, base)
        result = digitstring[d] + result
        length -= 1
    return result or '0'

def checkSet(c1, c2, c3, clever=True):
    """check if s,t,u form a set
    """
    # quick optimization: this more than doubles the speed of basic and legible!
    if clever and (c1 + c2 + c3) % 3: return False
    props = zip(toBase(c1, 3, 4),  toBase(c2, 3, 4), toBase(c3, 3, 4))
    return all(len(set(p)) in (1,3) for p in props)

def dumb(table):
    """The simplrst and slowest version
    """
    for i1,i2,i3 in combinations(range(len(table)), 3):
        if checkSet(table[i1], table[i2], table[i3], False):
            return i1, i2, i3

def basic(table):
    """naive version:
    three loops, check if cards form a set
    """
    for i1 in range(len(table)):
        for i2 in range(i1 + 1, len(table)):
            for i3 in range(i2 + 1, len(table)):
                if checkSet(table[i1], table[i2], table[i3]):
                    return i1, i2, i3

def legible(table):
    """use combinations to pick cards from the table
    uses fewer loops than basic, and faster version of checkSet
    """
    for i1,i2,i3 in combinations(range(len(table)), 3):
        if checkSet(table[i1], table[i2], table[i3]):
            return i1, i2, i3

def precode(table):
    """If you realise that checkSet spends most of its time
    in the toBase function, this is the obvious way to go
    """
    # decode and number the cards
    cards = enumerate(
        toBase(c, 3, 4)
        for c in table)
    # pick three cards
    for nc1, nc2, nc3 in combinations(cards, 3):
        # get place on table, and the three decoded cards
        ids, (c1, c2, c3) = zip(nc1, nc2, nc3)
        # get the four properties of the set of cards
        p1, p2, p3, p4 = zip(c1, c2, c3)
        # check if the property occurs once or thrice
        if (len(set(p1)) != 2
                and len(set(p2)) != 2
                and len(set(p3)) != 2
                and len(set(p4)) != 2):
            return ids

def precode2(table):
    """Uses clever trick to check for validity
    (Note that for each property, the sum of the value is
    a multiple of 3 iff they match as a set!)
    """
    # decode and number the cards
    cards = enumerate(
        (c//27, c//9%3, c//3%3, c%3)
        for c in table)
    # pick three cards
    for nc1, nc2, nc3 in combinations(cards, 3):
        # get place on table, and the three decoded cards
        ids, (c1, c2, c3) = zip(nc1, nc2, nc3)
        # get the four properties of the set of cards
        # note that the obvious if not any(sum(p)%3 for p in zip... is slow!
        p1, p2, p3, p4 = zip(c1, c2, c3)
        # check if the property occurs once or thrice
        if not(sum(p1)%3 or sum(p2)%3 or sum(p3)%3 or sum(p4)%3):
            return ids

def nozip(table):
    """Same as precode2, but doesn't use zip.
    """
    # decode and number the cards
    cards = enumerate(
        (c//27, c//9%3, c//3%3, c%3)
        for c in table)
    # pick three cards, unpack everything
    for ((id0, (c0p0, c0p1, c0p2, c0p3)),
         (id1, (c1p0, c1p1, c1p2, c1p3)),
         (id2, (c2p0, c2p1, c2p2, c2p3))) in combinations(cards, 3):
        # check for set, explicitly
        if not((c0p0 + c1p0 + c2p0)%3 or
               (c0p1 + c1p1 + c2p1)%3 or
               (c0p2 + c1p2 + c2p2)%3 or
               (c0p3 + c1p3 + c2p3)%3):
            return id0, id1, id2

def backwards(table):
    """most sets are at the end, use that
    """
    result = precode(table[::-1])
    if result is None: return
    lt = len(table) - 1
    return tuple(lt - id for id in result[::-1])

# giant table of allowed triples
allowed = {
    tuple(int(''.join(card), 3)
          for card in zip(*cs))
    for cs in product('000 111 222 012 021 102 120 201 210'.split(), repeat=4)
    }
for c in range(81): allowed.remove((c,c,c))

def lookup(table):
    """there's only a limited amount of combinations,
    just search for them
    """
    for triple in combinations(table[::-1], 3):
        if triple in allowed:
            return [table.index(c) for c in triple[::-1]]

def lookupF(table):
    """there's only a limited amount of combinations
    hide loop by using filter, speeding it up a bit
    """
    try:
        triple = next(filter(allowed.__contains__, combinations(table[::-1], 3)))
        return [table.index(c) for c in triple[::-1]]
    except StopIteration: return

thirdTable = {
    (a,b): c
    for a, b, c in allowed}

def third(table):
    """Check if third of a pair is on the table.
    Carefully uses the table backwards, skipping the first element
    """
    for pair in combinations(table[:0:-1], 2):
        t = thirdTable[pair]
        if t in table:
            return sorted(
                table.index(c)
                for c in (*pair, t))

def thirdSet(table, 
        thirdTable=thirdTable):
    """Use set membership, and local var, and one less assignment
    removes sorted from result
    """
    setTable = set(table)
    for pair in combinations(table[:0:-1], 2):
        if thirdTable[pair] in setTable:
            return [table.index(c)
                    for c in (thirdTable[pair], pair[1], pair[0])]

thirdTableL = [[None] * 81 for i in range(81)]
for a, b, c in allowed:
    thirdTableL[a][b] = c

def thirdSetL(table):
    """Uses all the cleverness of thirdSet,
    encoding the thirdTable as a list of lists,
    so you can pre-fetch a sublist
    Also optimize the return statement:
    no more sorting of result, and prevent an index calc for a
    """
    setTable = set(table)
    for i in range(len(table) - 1, 1, -1):
        a = table[i]
        subList = thirdTableL[a]
        for b in table[i - 1:0:-1]:
            if subList[b] in setTable:
                return table.index(subList[b]), table.index(b), i

# variant of thirdTable only doing sorted sets; 6 times smaller
leanTable = {
    (a,b): c
    for a, b, c in allowed
    if a < b < c}

def lean(table):
    """using small table above, saving memory with respect to third
    """
    st = sorted(table)
    setTable = set(st[2:])
    for pair in combinations(st[:-1], 2):
        if pair not in leanTable: continue
        t = leanTable[pair]
        if t in setTable:
            return sorted(
                table.index(c)
                for c in (*pair, t))

def leanF(table):
    """using small table above
    same as lean, shows filter doesn't always help
    """
    st = sorted(table)
    setTable = set(st[2:])
    for pair in filter(
            leanTable.__contains__,
            combinations(st[:-1], 2)):
        t = leanTable[pair]
        if t in setTable:
            return sorted(
                table.index(c)
                for c in (*pair, t))

def leanT(table):
    """using small table above, using try block
    slightly slower, because of the try block
    """
    st = sorted(table)
    setTable = set(st[2:])
    for pair in combinations(st[:-1], 2):
        try:
            t = leanTable[pair]
        except KeyError:
            continue
        if t in setTable:
            return sorted(
                table.index(c)
                for c in (*pair, t))

# in between thirdTable and leanTable
leanTable4 = {
    (a,b): c
    for a, b, c in allowed
    if a < b}

def lean4(table):
    """using sightly bigger table above, saving a check
    """
    st = sorted(table)
    setTable = set(st[2:])
    for pair in combinations(st[:-1], 2):
        t = leanTable4[pair]
        if t in setTable:
            return sorted(
                table.index(c)
                for c in (*pair, t))

combArray = np.array(
    list(combinations(range(12), 2)),
    dtype=np.uint8)
thirdArray = np.fromiter(
    (thirdTable.get(p, p[0])
	    for p in product(range(81), repeat=2)),
	   dtype=np.uint8)
thirdArray.shape = 81,81

def numpy(table):
    """Computes a bit too much, but has simple code
    """
    table = np.array(table, dtype=np.uint8)
    third = thirdArray[tuple(table[combArray].T)]
    c, t = (table[np.newaxis,:]==third[:,np.newaxis]).nonzero()
    if len(t): return (*combArray[c[0]], t[0])
    
def numpy2(table):
    """Try to prevent computation
    """
    table = np.array(table, dtype=np.uint8)
    third = thirdArray[tuple(table[combArray].T)]
    # find common elements in table and third
    tableIter = iter(sorted(enumerate(table), key=itemgetter(1)))
    thirdIter = iter(sorted(enumerate(third), key=itemgetter(1)))
    tai, tav = next(tableIter)
    thi, thv = next(thirdIter)
    try:
        while True:
            if tav < thv: tai, tav = next(tableIter)
            elif thv < tav: thi, thv = next(thirdIter)
            else: break
        return sorted((*combArray[thi], tai))
    except StopIteration: return

setTable = tuple(set(map(frozenset, allowed)))
bigTable = defaultdict(int)
for a,b,c in allowed:
    bigTable[a] |= 1 << 2 * setTable.index(frozenset({a,b,c}))
bigMask = (1 << 2160) // 3

def bitMap(table):
    """make a bitmap
    and match it with allowed maps
    there are 9**4=6561 of these
    you can eliminate using known missing cards
    if a set ID is two cards, the set has three IDS
    New idea:
    make a bitmap (as index from card to sets, 2 bits for each set
    there are 1080 sets, so these are 2160 bits numbers
    add up the values and with 0xaaaaa....
    if this is nonzero, you have a set
    """
    total = sum(bigTable[c] for c in table)
    match =  total & total >> 1 & bigMask
    if match:
        values = setTable[match.bit_length() >> 1]
        return sorted(table.index(c)
                          for c in values)

# for each card, remember the sets in which it occurs
setsPerCard = defaultdict(list)
for s in setTable:
    for c in s:
        setsPerCard[c].append(s)

def count(table):
    """Use setsPerCard to simply count the sets that
    contain cards on the table
    Any set occuring three times is OK
    """
    total = Counter(chain.from_iterable(map(setsPerCard.get, table)))
    (m, c), = total.most_common(1)
    if c == 3:
        return sorted(table.index(c)
                      for c in m)

def count2(table):
    """Like count, but use early exit
    Is slower, because you do too many tests,
    but mainly because Counter(iterable) uses a builtin function (!)
    """
    counter = defaultdict(int)
    counterGet = counter.get
    for s in chain.from_iterable(map(setsPerCard.get, table)):
        counter[s] += 1
        if counter[s] == 3:
            return sorted(table.index(c)
                           for c in s)

def split(table,
        thirdTable=thirdTable):
    # split the table in three groups
    tables = [set(), set(), set()]
    for c in table:
        tables[c%3].add(c)
    if not all(tables):
        # resplit if too lopsided
        tables = [set(), set(), set()]
        for c in table:
            tables[c//27].add(c)
    tables1, tables2 = tables[1:]
    for a in tables[0]:
        tableA = thirdTableL[a]
        for b in tables1:
            if tableA[b] in tables2:
                return sorted(
                    table.index(c)
                    for c in (a, b, thirdTable[a, b]))
    for pt in tables:
        for pair in combinations(pt, 2):
            if thirdTable[pair] in pt:
                return sorted(
                    table.index(c)
                    for c in (*pair, thirdTable[pair]))

third9 = {(0,1):2, (0,3):6, (0,4):8, (0,5):7,
          (1,3):8, (1,4):7, (1,5):6,
          (2,3):7, (2,4):6, (2,5):8}

def split9(table):
    # split the table in nine groups
    tables = [[] for i in range (9)]
    for c in table:
        tables[c%9].append(c)
    for (i,j),k in third9.items():
        if not tables[i]: continue
        tablesk = tables[k]
        for pair in product(tables[i], tables[j]):
            if thirdTable[pair] in tablesk:
                return sorted(
                    table.index(c)
                    for c in (*pair, thirdTable[pair]))
    for t in tables:
        for s in combinations(t, 3):
            if s in allowed:
                return sorted(table.index(c)
                              for c in s)
def printTable(table):
    for i in range(0, len(table), 4):
        print('  ', *(map(toBase, table[i:i+4], repeat(3), repeat(4))))

def play(function, games=1):
    if games > 1: print("Performance for:", function.__name__)
    totalTime, tries = {}, {}
    for i in range(games):
        deck = shuffleDeck()
        table = []
        while len(deck) > 11:
            while len(table) < 12: table.append(deck.pop())
            if games == 1:
                print("Table")
                printTable(table)
            while True:
                t = time.perf_counter()
                ids = function(table)
                t = time.perf_counter() - t
                if len(table) not in tries: 
                    totalTime[len(table)] = 0.
                    tries[len(table)] = 0
                totalTime[len(table)] += t
                tries[len(table)] += 1
                if not ids and deck:
                    table.append(deck.pop())
                    if games == 1: print ("------- Extra card:", toBase(table[-1], 3,4))
                else:
                    break
            found = [table.pop(id) for id in ids[::-1]][::-1]
            if games == 1:
                print("Found:", *map(toBase, found, repeat(3), repeat(4)))
    for s in tries:
        print(f"  {s} is {totalTime[s] / tries[s] * 1e6:.1f} us")
    return totalTime[12] / tries[12]

__test__ = {
'all': """
    >>> random.seed(12)
    >>> table = shuffleDeck()[:12]
    >>> for f in allRoutines:
    ...    result = f(table)
    ...    check = checkSet(*map(table.__getitem__, result))
    ...    print(f.__name__.ljust(9), result, check)
    dumb      (0, 2, 5) True
    basic     (0, 2, 5) True
    legible   (0, 2, 5) True
    precode   (0, 2, 5) True
    precode2  (0, 2, 5) True
    nozip     (0, 2, 5) True
    backwards (0, 7, 11) True
    lookup    [0, 7, 11] True
    lookupF   [0, 7, 11] True
    third     [0, 7, 11] True
    thirdSet  [0, 7, 11] True
    thirdSetL [0, 7, 11] True
    lean      [3, 7, 9] True
    leanF     [3, 7, 9] True
    leanT     [3, 7, 9] True
    lean4     [3, 7, 9] True
    numpy     (0, 2, 5) True
    numpy2    [3, 7, 9] True
    bitMap    [4, 6, 10] True
    count     [0, 7, 11] True
    count2    [0, 2, 5] True
    split     [4, 6, 10] True
    split9    [3, 7, 9] True
""",
'compare': """
    >>> random.seed()
    >>> table = shuffleDeck()[:12]
    >>> try:
    ...    for f in allRoutines:
    ...       a,b,c = f(table)
    ...       if (table[a], table[b],table[c]) not in allowed:
    ...          print(f)
    ...          printTable(table)
    ... except TypeError: pass
""",
'play': """
    >>> random.seed(12)
    >>> play(basic) #doctest: +ELLIPSIS
    Table
       2020 1021 2111 1122
       0200 1210 0001 1202
       2021 1022 2011 1002
    Found: 1021 0001 2011
    Table
       2020 2111 1122 0200
       1210 1202 2021 1022
       1002 0000 2211 2002
    Found: 2111 0200 1022
    ...
""",
}

def setupDoctest():
    import ast
    parsed = ast.parse(open(__file__).read(), "doctest")
    doctypes = ast.Module, ast.FunctionDef, ast.ClassDef
    for node in ast.walk(parsed):
        if isinstance(node, doctypes):
            d = ast.get_docstring(node, True)
            if d:
                if getattr(node, "name", "module") in __test__: print(node.name)
                __test__[getattr(node, "name", "module")] = d

allRoutines = [
    dumb, basic, legible, precode, precode2, nozip,
    backwards, lookup, lookupF,
    third, thirdSet, thirdSetL,
    lean, leanF, leanT, lean4,
    numpy, numpy2,
    bitMap, count, count2, split, split9
    ]


if __name__=="__main__":
    try:
        assert False
        setupDoctest()
    except AssertionError: pass
    import doctest
    doctest.testmod()
    
    allTables = "allowed thirdTable thirdTableL leanTable leanTable4 combArray thirdArray setTable bigTable setsPerCard".split()
    print("Table sizes according to pickle:")
    for t in allTables:
        print("{:<12s}: {:5d}".format(
            t,
            len(dumps(globals()[t])),
            ))
    print()

    # you can supply a repeat count at the command line
    import sys
    repeatCount = int(sys.argv[1]) if len(sys.argv) > 1 else 25
    summary = {}
    basicTime = 1
    for f in allRoutines:
        random.seed(12)
        summary[f] = play(f, repeatCount)
    for f,perf in sorted(summary.items(), key=itemgetter(1)):
        print("Speedup for {:<10s}{:5.1f} ({})".format(
            	  f.__name__ + ':',
            	  summary[basic] / perf,
            	  ','.join(set(f.__code__.co_names) & set(allTables))
            	  ))
